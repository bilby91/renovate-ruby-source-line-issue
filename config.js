module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  token: process.env.CI_JOB_TOKEN,
  logLevel: 'info',
  requireConfig: false,
  onboarding: false,
  repositories: [
    'bilby91/renovate-ruby-source-line-issue',
  ],
  onboardingConfig: {
    extends: ['config:base'],
    prConcurrentLimit: 5,
  },
  enabledManagers: [
    'bundler',
    'dockerfile'
  ],
};
