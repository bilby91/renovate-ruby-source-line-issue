FROM ruby:2.6.4-alpine as runtime

ENV HOME /root
ENV APP_HOME /var/www/app

# Install required packages
RUN apk add --no-cache \
    bash \
    build-base \
    curl-dev \
    git \
    linux-headers \
    postgresql-client \
    postgresql-dev \
    tzdata \
    tini

# Set the working directory
WORKDIR $APP_HOME

# Install AWS CLI
RUN apk -v --update add \
        python \
        py-pip \
        groff \
        less \
        mailcap \
        python2-dev \
        && \
    pip install --upgrade awscli s3cmd python-magic && \
    apk -v --purge del py-pip && \
    rm /var/cache/apk/*

FROM runtime as application

# Add the Gemfile and Gemfile.lock into the image
ADD Gemfile* $APP_HOME/

# Install ruby dependencies
RUN bundle install

# COPY app to container
COPY . $APP_HOME

EXPOSE 3000

ENTRYPOINT [ "tini", "--" ]

CMD [ "bundle", "exec", "puma", "-C", "config/puma.rb" ]
